echo "Start provisioning..."
date > /etc/vagrant_provisioned_at

sudo apt-get update -y
sudo apt-get install git -y

sudo apt-get -y install docker.io

# Link and fix paths with the following two commands:
ln -sf /usr/bin/docker.io /usr/local/bin/docker
sed -i '$acomplete -F _docker docker' /etc/bash_completion.d/docker.io

# Finally, and optionally, let’s configure Docker to start when the server boots:
update-rc.d docker.io defaults

# Install docker-compose
sudo curl -L "https://github.com/docker/compose/releases/download/1.26.0/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose
sudo chmod +x /usr/local/bin/docker-compose
sudo docker-compose --version

# https://github.com/gawbul/docker-sge
sudo docker run -it --rm gawbul/docker-sge login -f sgeadmin

echo "...End provisioning"